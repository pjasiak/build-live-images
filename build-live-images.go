package main

import (
    "crypto/sha256"
    "encoding/json"
    "encoding/hex"
    "fmt"
    "io"
    "io/ioutil"
    "log"
    "os"
    "os/exec"
    "strings"
)

var (
    Info        *log.Logger
    Warning     *log.Logger
    Error       *log.Logger
    LB          =  "/usr/bin/lb"
    ConfigPath  =  "/usr/local/etc/build-live-images"
)

type Config struct {
    Log         string  `json:"Log"`
    BuildRoot   string  `json:"BuildRoot"`
    Images      string  `json:"Images"`
    Dest        string  `json:"Dest"`
}

func load_config(file string) Config {
    var config Config
    config_file, err := ioutil.ReadFile(file)
    if err != nil {
        fmt.Println(err.Error())
        os.Exit(1)
    }
    if err = json.Unmarshal(config_file, &config); err != nil {
        fmt.Println(err.Error())
        os.Exit(1)
    }
    return config
}

func init_log(config Config) {
    config_log, err := os.OpenFile(config.Log, os.O_RDWR | os.O_APPEND, os.FileMode(0644))
    if err != nil {
        fmt.Println(err.Error())
        os.Exit(1)
    }
    log_file := io.MultiWriter(config_log, os.Stdout)
    Info = log.New(log_file, "INFO: ", log.Ldate | log.Ltime | log.Lshortfile)
    Warning = log.New(log_file, "WARNING: ", log.Ldate | log.Ltime | log.Lshortfile)
    Error = log.New(log_file, "ERROR: ", log.Ldate | log.Ltime | log.Lshortfile)
}

func clean(config Config, image string) {
    Info.Printf("clean " + config.BuildRoot + "/" + image)
    cmd := exec.Command(LB, "clean")
    cmd.Dir = config.BuildRoot + "/" + image + "/"
    if err := cmd.Run(); err != nil {
        Error.Println(err.Error())
        os.Exit(1)
    }
}

func build(config Config, image string) {
    Info.Printf("start building %v\n", image)
    cmd := exec.Command(LB, "build")
    cmd.Dir = config.BuildRoot + "/" + image + "/"
    if err := cmd.Run(); err != nil {
        Error.Println(err.Error())
        os.Exit(1)
    }
    Info.Printf("build complete for %v\n", image)
}

func sha256sum(file string) string {
    Info.Printf("calculate sha256sum for %v\n", file)
    f, err := os.Open(file)
    if err != nil {
        Error.Println(err.Error())
        os.Exit(1)
    }
    defer f.Close()
    h := sha256.New()
    if _, err := io.Copy(h, f); err != nil {
        Error.Println(err.Error())
        os.Exit(1)
    }
    return hex.EncodeToString(h.Sum(nil))
}

func move(config Config, image string) {
    src := config.BuildRoot + "/" + image + "/live-image-amd64.hybrid.iso"
    dest := config.Dest + "/" + image + ".iso"
    if err := os.Rename(src, dest); err != nil {
        Error.Println(err.Error())
        os.Exit(1)
    }
    Info.Printf("move %v to %v\n", src, dest)
    sum := sha256sum(dest)
    sum_dest, err := os.Create(dest + ".sha256sum")
    if err != nil {
        Error.Println(err.Error())
        os.Exit(1)
    }
    defer sum_dest.Close()
    sum_dest.Write([]byte(sum + "  " + dest))
}

func main() {
    config := load_config(ConfigPath)
    init_log(config)
    for _, image := range strings.Fields(config.Images) {
        clean(config, image)
        build(config, image)
        move(config, image)
        clean(config, image)
    }
    os.Exit(0)
}
