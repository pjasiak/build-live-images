all: build-live-images

install:
	install -m 644 config.json /usr/local/etc/build-live-images
	install -m 755 build-live-images /usr/local/sbin/build-live-images

build-live-images: build-live-images.go
	go build

clean:
	rm build-live-images

.PHONY: all clean
